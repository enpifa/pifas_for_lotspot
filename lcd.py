from camera_setup import CameraParameters
import subprocess
import datetime
import time

def check_hdmi_connection():
    # When HDMI is connected, 'tvservice -n' displays 'device_name = RTK-32V3H-H6A'
    # When disconnected, it displays '[E] No device present'
    # If the string is found, the find function returns the position in the string where it starts, between 0 and string_size
    output = subprocess.check_output("tvservice -n", shell=True, stderr=subprocess.STDOUT)
    if (output.find("No device present")>=0) :
        return False
    return True

def main():
    cp = CameraParameters()

    directory = "~/lot_spot_rapunzel/rapunzel/lotspot/lcd_calibrating/LCD-"

    try:
        # While the HDMI cable is connected, create a new image and display it on the LCD screen
        while (check_hdmi_connection()):
            # The following line needs to consider permission restrictions
            if not os.path.exists(directory):
                os.makedirs(directory)

            filename = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + ".jpg"
            cmd = "fswebcam " + directory + filename

            print ("The command is {}\nProcessing the image...".format(cmd))
            output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)

            output = subprocess.check_output("chmod 777 lcd_calibrating/*.jpg", shell=True, stderr=subprocess.STDOUT)

            print ("Opening image {} ...".format("lcd_calibrating/LCD-" + filename))
            time.sleep(1)

        print("Time to reboot!")

    except:
        pass

if __name__== "__main__":
    main()
