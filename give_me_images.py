"""
Module to test the different settings of the ELP camera.
"""

import subprocess
import datetime

def set_defaults():
    """
    Set defaults to start testing the parameters individually
    """
    cmd = "fswebcam --frames 20 --set 'Brightness'=12 --set 'Saturation'=20 --set 'Contrast'=0 --set 'Hue'=0 --set 'Exposure (Absolute)'=157 --set 'Gain'=0 --set 'White Balance Temperature'=4600 --set 'White Balance Temperature, Auto'='True' --set 'Gamma'=72 --set 'Power Line Frequency'='50 Hz' --set 'Sharpness'=0 --set 'Backlight Compensation'=1 --set 'Exposure, Auto'='Manual Mode' --set 'Exposure, Auto Priority'='True'"
    output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)

def customized():
    """
    Test brightness, saturation, contrast, exposure (absolute) and gain.
    Decisions:
    - turn off all the Auto parameters
    - set White Balance Temperature to low (3200), instead of medium
    Defaults:
    - changed: (bri=12, sat=20, con=0, exp=157, gain=0, white_balance=4600, white_balance_auto=True, gain=25, sharpness=0, exposure_priority=True)
    - same: (hue=0, gamma=72, power=50, backlight=1, exposure_auto)
    """
    bri = -5
    sat = 50
    con = 10
    exp = 50
    gain = 25
    cmd = "fswebcam --frames 20 --no-banner "
    cmd += "--set 'Brightness'={} --set 'Saturation'={} --set 'Contrast'={} --set 'Exposure (Absolute)'={} --set 'Gain'={} --set 'Sharpness'=5 ".format(bri, sat, con, exp, gain)
    cmd += "--set 'Hue'=0 --set 'White Balance Temperature'=3200 --set 'White Balance Temperature, Auto'='False' --set 'Gamma'=72 --set 'Power Line Frequency'='50 Hz' --set 'Backlight Compensation'=1 --set 'Exposure, Auto'='Manual Mode' --set 'Exposure, Auto Priority'='False' "
    cmd += "custom/new{}-bri{}-con{}-sat{}-gain{}-exp{}.jpg".format(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"), bri, con, sat, gain, exp)
    output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
    print (output)

def brightness():
    # -64 - 64
    set_defaults()

    b = -64
    counter = 1
    while (b < 64):
        cmd = "fswebcam --set '{}'='{}' --frames 20 brightness/brightness{}.jpg".format("Brightness", b, b)
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        print ("Round {}: {}".format(counter, cmd))
        b += 10
        counter += 1

def saturation():
    # 0 - 128
    set_defaults()

    s = 0
    counter = 1
    while (s < 128):
        cmd = "fswebcam --set '{}'='{}' --frames 20 saturation/saturation{}.jpg".format("Saturation", s, s)
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        print ("Round {}: {}".format(counter, cmd))
        s += 12
        counter += 1

def contrast():
    # 0 - 64
    set_defaults()

    c = 0
    counter = 1
    while (c < 64):
        cmd = "fswebcam --set '{}'='{}' --frames 20 contrast/contrast{}.jpg".format("Contrast", c, c)
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        print ("Round {}: {}".format(counter, cmd))
        c += 12
        counter += 1

def hue():
    # -40 - 40
    set_defaults()

    val = -40
    counter = 1
    while (val < 40):
        cmd = "fswebcam --set '{}'='{}' --frames 20 hue/hue{}.jpg".format("Hue", val, val)
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        print ("Round {}: {}".format(counter, cmd))
        val += 12
        counter += 1

def exposure_absolute():
    # 1 - 5000
    set_defaults()

    val = 1
    counter = 1
    while (val < 300):
        cmd = "fswebcam --set '{}'='{}' --frames 20 ExpAbs/expabs{}.jpg".format("Exposure (Absolute)", val, val)
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        print ("Round {}: {}".format(counter, cmd))
        val += 7
        counter += 1

def gain():
    # 1 - 5000
    set_defaults()

    val = 1
    counter = 1
    while (val < 100):
        cmd = "fswebcam --set '{}'='{}' --frames 20 gain/gain{}.jpg".format("Gain", val, val)
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        print ("Round {}: {}".format(counter, cmd))
        val += 9
        counter += 1

def white_balance():
    # 2800 - 6500
    set_defaults()

    val = 2800
    counter = 1
    while (val < 6500):
        cmd = "fswebcam --set '{}'='{}' --frames 20 white_balance/white_balance{}.jpg".format("White Balance Temperature", val, val)
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        print ("Round {}: {}".format(counter, cmd))
        val += 211
        counter += 1

def white_balance_auto():
    # True - False
    set_defaults()

    val = "True"
    cmd = "fswebcam --set '{}'='{}' --frames 20 white_balance/white_balance{}.jpg".format("White Balance Temperature, Auto", val, val)
    output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
    print ("Round 1: {}".format(cmd))

    val = "False"
    cmd = "fswebcam --set '{}'='{}' --frames 20 white_balance/white_balance{}.jpg".format("White Balance Temperature, Auto", val, val)
    output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
    print ("Round 2: {}".format(cmd))

def gamma():
    # 72 - 500
    set_defaults()

    val = 72
    counter = 1
    while (val < 500):
        cmd = "fswebcam --set '{}'='{}' --frames 20 gamma/gamma{}.jpg".format("Gamma", val, val)
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        print ("Round {}: {}".format(counter, cmd))
        val += 71
        counter += 1

def power():
    # Disabled, 50 Hz, 60 Hz
    set_defaults()

    val = "Disabled"
    cmd = "fswebcam --set '{}'='{}' --frames 20 power/power{}.jpg".format("Power Line Frequency", val, val)
    output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
    print ("Round 1: {}".format(cmd))

    val = "50 Hz"
    cmd = "fswebcam --set '{}'='{}' --frames 20 power/power{}.jpg".format("Power Line Frequency", val, "50")
    output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
    print ("Round 2: {}".format(cmd))

    val = "60 Hz"
    cmd = "fswebcam --set '{}'='{}' --frames 20 power/power{}.jpg".format("Power Line Frequency", val, "60")
    output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
    print ("Round 3: {}".format(cmd))

def sharpness():
    # 72 - 500
    set_defaults()

    val = 0
    counter = 1
    while (val < 7):
        cmd = "fswebcam --set '{}'='{}' --frames 20 sharpness/sharpness{}.jpg".format("Sharpness", val, val)
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        print ("Round {}: {}".format(counter, cmd))
        val += 1
        counter += 1

def backlight():
    # 0,1,2
    set_defaults()

    val = 0
    counter = 1
    while (val < 3):
        cmd = "fswebcam --set '{}'='{}' --frames 20 backlight/backlight{}.jpg".format("Backlight Compensation", val, val)
        output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        print ("Round {}: {}".format(counter, cmd))
        val += 1
        counter += 1

def exposure_auto():
    # Disabled, 50 Hz, 60 Hz
    set_defaults()

    val = "Manual Mode"
    cmd = "fswebcam --set '{}'='{}' --frames 20 expauto/expauto{}.jpg".format("Exposure, Auto", val, "manualmode")
    output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
    print ("Round 1: {}".format(cmd))

    val = "Aperture Priority Mode"
    cmd = "fswebcam --set '{}'='{}' --frames 20 expauto/expauto{}.jpg".format("Exposure, Auto", val, "apertureprioritymode")
    output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
    print ("Round 2: {}".format(cmd))

def exposure_priority():
    # Disabled, 50 Hz, 60 Hz
    set_defaults()

    val = "True"
    cmd = "fswebcam --set '{}'='{}' --frames 20 exppriority/exppriority{}.jpg".format("Exposure, Auto Priority", val, val)
    output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
    print ("Round 1: {}".format(cmd))

    val = "False"
    cmd = "fswebcam --set '{}'='{}' --frames 20 exppriority/exppriority{}.jpg".format("Exposure, Auto Priority", val, val)
    output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
    print ("Round 2: {}".format(cmd))

def main():
    # brightness()
    # saturation()
    # contrast()
    # hue()
    # exposure_absolute()
    # gain()
    # white_balance()
    # white_balance_auto()
    # gamma()
    # power()
    # sharpness()
    # backlight()
    # exposure_auto()
    # exposure_priority()
    customized()

if __name__== "__main__":
    main()
