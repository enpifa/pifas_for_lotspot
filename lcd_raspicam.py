"""
This module allows to set the camera's position by streaming on the LCD screen.
It streams until the developer decides to exit or the HDMI is disconnected.
Operator execution (no argument): "python lcd_raspicam.py"
Dev execution (one argument): "python lcd_raspicam.py Your_name"
"""
import subprocess
import datetime
import time
import sys

def check_hdmi_connection():
    # When HDMI is connected, 'tvservice -n' displays 'device_name = RTK-32V3H-H6A'
    # When disconnected, it displays '[E] No device present'
    # It is important to comment the line 'hdmi_force_hotplug=1' from the file /boot/config.txt
    # It is also important to set 'config_hdmi_boost=4'. This increases the signal strength of the HDMI interface
    # If the string is found, the find function returns the position in the string where it starts, between 0 and string_size
    output = subprocess.check_output("tvservice -n", shell=True, stderr=subprocess.STDOUT)
    if (output.find("No device present")>=0) :
        return False
    return True

def set_camera_position(development = False):
    """
    Verify the program is called with 1 (operator) or 2 (developer) arguments.
    Stream on LCD screen in sets of 10 seconds.
    """
    directory = "~/lot_spot_rapunzel/rapunzel/lotspot/lcd_raspicam/Raspicam-"

    timeout = 60000
    if development is True:
        timeout = 10000
        print ("Welcome, developer")

    # While the HDMI cable is connected, create a new image and display it on the LCD screen
    while (check_hdmi_connection()):
        # The following line needs to consider permission restrictions
        # if not os.path.exists(directory):
        #     os.makedirs(directory)

        filename = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + ".jpg"
        # -t: allows to display the camera stream for a certain amount of miliseconds
        # -k: allows to capture a photo every time the user presses ENTER
        # -o: output of the captured image.
        # When the video streaming finishes, it captures a phone and saves it.
        #  -o " + directory + filename
        cmd = "raspistill -t {} -v -o {}{}".format(timeout, directory, filename)

        print ("The command is '{}'\nProcessing the image...".format(cmd))

        start_time = time.time()

        try:
            output = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            raise RuntimeError("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))

        end_time = time.time()
        total_exec_time = end_time - start_time
        print ("Execution time: {}".format(total_exec_time))

        if (total_exec_time < (timeout/1005)) :
            print ("Camera focus test ended earlier...")

        if development is True:
            topic = raw_input("Enter something+ENTER to exit, ENTER to run it again:")
            if (topic != ""): break

    print("Time to reboot!")

def run(development = True):
    set_camera_position(development = development)

def main():
    run()

if __name__== "__main__":
    main()
