
def verify_config():

    print("Entering...")
    file = open("hola.txt","r+")
    lines = file.readlines()
    file.seek(0)
    hotplug_set = False
    boost_set = False
    for line in lines:
        if line.find("hdmi_force_hotplug") >= 0:
            if hotplug_set is False:
                file.write("hdmi_force_hotplug=1\n")
                print("Found HDMI_FORCE_HOTPLUG, set to 1.")
                hotplug_set = True
            else:
                print("Removed duplicated of HDMI_FORCE_HOTPLUG.")
        elif line.find("config_hdmi_boost") >= 0:
            if boost_set is False:
                file.write("config_hdmi_boost=4\n")
                print("Found CONFIG_HDMI_BOOST, set to 4.")
                boost_set = True
            else:
                print("Removed duplicated of CONFIG_HDMI_BOOST.")
        else:
            file.write(line)

    if hotplug_set is False:
        file.write("hdmi_force_hotplug=1\n")

    if boost_set is False:
        file.write("config_hdmi_boost=4\n")

    file.truncate()
    file.close()

def main():
    verify_config()

if __name__== "__main__":
    main()
